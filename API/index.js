const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user.js")
const productRoutes = require("./routes/product.js")
const orderRoutes = require("./routes/order.js")

const app = express();

//DB Connection
mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.21yyaiu.mongodb.net/E-Commerce-Shoes?retryWrites=true&w=majority", {
	useNewUrlParser: true, 
	useUnifiedTopology: true
})



//Prompts message in the terminal once connecton is open
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"))


app.use('/uploads', express.static('uploads'))
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Initializing the routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);


app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
})
