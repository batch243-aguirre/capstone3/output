import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import bannerR from "../images/bannerR.png"
import {Container, Row, Col } from 'react-bootstrap';


export default function Basketball() {


	const [basketball, setBasketball] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/basketball`)
		.then(res => res.json())
		.then(data => {
			
			console.log(data);
			setBasketball(data.map(basketball =>{
				return(
					<ProductCard key={basketball._id} productProp={basketball}/>
				);
			}));
		})
	}, []);

	return(

		<>
		<Row>
		<img src={bannerR} alt=""/>
		</Row>
		<Container className="main-container ">
		<Row>
		  <h1 className="text-center ff-bold p-1">Basketball Shoes</h1>
		</Row>
		<Row className="products-row p-1">
		  {basketball.map((shoe, index) => (
		    <Col xs={12} md={6} lg={4} className="p-1" key={index}>
		      {shoe}
		    </Col>
		  ))}
		</Row>
		</Container>
		</>
	)
}