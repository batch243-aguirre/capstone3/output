import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";

import {Container, Row, Col } from 'react-bootstrap';
import bannerN from "../images/bannerN.png"


export default function Running() {

	const [running, setRunning] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/running`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setRunning(data.map(running =>{
				return(
					<ProductCard key={running._id} productProp={running}/>
				);
			}));
		})
	}, []);

	return(
		<>
		<Row>
		<img src={bannerN} alt=""/>
		</Row>
		<Container className="main-container ">
		
		<Row><h1 className="text-center ff-bold p-1">Running Shoes</h1></Row>
		<Row className="products-row p-1">
		  {running.map((shoe, index) => (
		    <Col xs={12} md={6} lg={4} className="p-1" key={index}>
		      {shoe}
		    </Col>
		  ))}
		</Row>
		</Container>
		</>
	)
}