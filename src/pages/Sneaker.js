import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import bannerE from "../images/bannerE.png"

import {Container, Row, Col } from 'react-bootstrap';


export default function Sneaker() {


	const [sneaker, setSneaker] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/sneaker`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setSneaker(data.map(sneaker =>{
				return(
					<ProductCard key={sneaker._id} productProp={sneaker}/>
				);
			}));
		})
	}, []);

	return(
		<>
		<Row>
		<img src={bannerE} alt=""/>
		</Row>
		<Container className="main-container ">
		<Row><h1 className="text-center ff-bold p-1">Sneakers</h1></Row>
		<Row className="products-row p-1">
		  {sneaker.map((shoe, index) => (
		    <Col xs={12} md={6} lg={4} className="p-1" key={index}>
		      {shoe}
		    </Col>
		  ))}
		</Row>
		</Container>
		</>
	)
}