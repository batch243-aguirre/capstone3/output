import { useState, useEffect } from "react";

import {Container, Card, Button, Row, Col, InputGroup} from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function ProductView(){
    const { productId } = useParams();
    const navigate = useNavigate();
    const [productName, setProductName] = useState('');
    const [productImage, setProductImage] = useState('');
    const [productType, setProductType] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(1);
    const [totalAmount, setTotalAmount] = useState(0);
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    useEffect(() => {
        // Check if user is logged in
        const token = localStorage.getItem('token');
        if (token) {
            setIsLoggedIn(true);
        }

        console.log(productId);

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setProductName(data.productName);
                setProductType(data.productType);
                setProductImage(data.productImage);
                setDescription(data.description);
                setPrice(data.price);
                setTotalAmount(data.price * quantity);
            });
    }, [productId]);

    const handleCheckout = () => {
        if (!isLoggedIn) {
            navigate('/login');
        } else {
            fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    productId: productId,
                    quantity: quantity
                })
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    if (data) {
                        Swal.fire({
                            title: "Successfully Ordered",
                            icon: "success",
                            text: "You have successfully placed an order.",
                            confirmButtonColor: "black"
                        });
                        navigate("/products");
                    } else {
                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "You are Admin,You are not allowed to checkout",
                            confirmButtonColor: "black"
                        });
                    }
                });
        }
    }

    const increment = () => {
        setQuantity((prevCount) => prevCount + 1);
    }

	const decrement = (quantity) => {
	setQuantity((prevCount) => {
			if (prevCount > 1) {
		        return (prevCount -= 1); 
		    } else {
		        return (prevCount = 1);
		      }
		})
	}





	return(


		<Container className="main-container">
            <Row>
            <Col lg={{ span: 8, offset: 2 }}>
            <Card className="d-flex d-column">
              <Row className=" mx-auto">
                <Col  md={8}>
                  <Card.Img variant="top" src={`${process.env.REACT_APP_API_URL}/${productImage}`} />
                </Col>
                <Col md={4}>
                  <Card.Body>
                    <Card.Title>{productName}</Card.Title>
                    <Card.Subtitle>Type: {productType}</Card.Subtitle>
					<Card.Subtitle>Description: {description}</Card.Subtitle>
					<Card.Text>
						<InputGroup>
					      <Button variant="outline-secondary" onClick={decrement}>-</Button>
					      <InputGroup.Text>{quantity}</InputGroup.Text>
					        <Button variant="outline-secondary" onClick={increment}>+</Button>
					      </InputGroup>
					</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP {price}</Card.Text>
					<Card.Subtitle>Total:</Card.Subtitle>
					<Card.Text type="Number">{totalAmount*quantity}</Card.Text>
					{isLoggedIn ? 
            <Button variant="dark"  size="lg" onClick={handleCheckout}>Checkout</Button>:
					<Button variant="dark"  size="lg" onClick={()=>navigate('/login')}>Log in to checkout</Button>}
                  </Card.Body>
                </Col>
              </Row>
            </Card>
            </Col>
            </Row>
            </Container>
	)
}
