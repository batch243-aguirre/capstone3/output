import { useState, useEffect, useContext } from 'react';
import { Navigate } from "react-router-dom";
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import googleIcon from "../icons/google.png";
import fbIcon from "../icons/fb.png";



export default function Login(){

    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')

    const [isActive, setIsActive] = useState(false);


useEffect(() => {

    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

}, [email, password]);

const authenticate = async (e) => {
    e.preventDefault();

    try {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        });
        const data = await res.json();
        if(data.access !== undefined){
            localStorage.setItem("token", data.access);
            await retrieveUserDetails(data.access);

            Swal.fire({
                title: "Login Successful",
                icon: "success",
                text: "Welcome to Shoe Marikina",
                showConfirmButton: false,
                timer: 1500
            });
        }
    } catch (error) {
        Swal.fire({
            title: "Authentication Failed!",
            icon: "error",
            text: "Check your login details and try again.",
            confirmButtonColor: "black"
        });
    }

    setEmail('');
    setPassword('');
};

const retrieveUserDetails = async (token) => {
    try {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method: "POST",
            headers:{
                Authorization: `Bearer ${token}`
            }
        });
        const data = await res.json();
        console.table(data);

        setUser({
            id: data._id,
            isAdmin: data.isAdmin
        });
    } catch (error) {
        console.log(error);
    }
}


return (

    (user.id !== null)
    ?
        <Navigate to="/products" />
    :

    <Container className="main-container login-container" fluid>
        <Row className="login-row">
            <Col md={8} lg={6}  className="login-img-col p-0">
                                           
            </Col>
            <Col md={4} lg={6}  className="form-col">
                <Row>
                    <h2 className="mt-5 mb-3 ff-bold text-center">LOGIN TO YOUR ACCOUNT</h2>
                </Row>
                <Row >
                    <span className="text-center py-3">Connect to your social media accounts to log in</span>
                </Row >
                <Row >
                    <Col md={12} className=" h-10 d-flex justify-content-center py-3">
                         <img src={googleIcon} alt="" className="p-1"/>
                         <img src={fbIcon} alt="" className="p-1"/>   
                    </Col>
                </Row>
                <Row className="text-center">
                    <span> ⎯⎯⎯⎯⎯ or ⎯⎯⎯⎯⎯ </span>
                </Row>
                <Row className=" px-5">
                    <Col className=" mx-auto px-5 login-col" >

                        <Form onSubmit={(e) => authenticate(e)}>
                            <Form.Group controlId="userEmail" className="mb-3">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="Enter email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="password" className="mb-3">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            {
                            isActive
                            ?
                                <Button type="submit" id="submitBtn" variant="dark">
                                    Login
                                </Button>
                            :
                                <Button type="submit" id="submitBtn" variant="light" disabled>
                                    Login
                                </Button>
                            }
                        </Form>
                    </Col> 
                </Row>
            </Col>
        </Row>
    </Container>
)
}
