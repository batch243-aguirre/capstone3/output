import { Card, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";

export default function ProductCard({productProp}){

	const { _id, productName, productType, description, price, productImage} = productProp;

	return (




     <Card className="my-3 p-3" border="light">
            <Card.Body className="p-3 d-flex flex-column">
                <Card.Title className="ff-bold">
                    {productName}
                </Card.Title>
                <Card.Img src={`${process.env.REACT_APP_API_URL}/${productImage}`} />
                <Card.Text className="ff-reg ">
                   Type: {productType}
                </Card.Text>
                <Card.Text className="ff-reg">
                    Description: {description}
                </Card.Text>
                <Card.Text className="ff-reg">
                    Price: Php {price}
                </Card.Text>
                <Button as={Link} to={`/products/${_id}`} variant="dark" className="mt-auto" style={{ backgroundColor: 'black', color: 'white',  borderRadius: '50px 15px' }}>Details</Button>
            </Card.Body>
        </Card>


		)
}