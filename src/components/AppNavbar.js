import { NavLink } from "react-router-dom";
import { useState,  useContext } from "react";
import { Container, Row, Col, Nav, Navbar } from "react-bootstrap";
import UserContext from "../UserContext";

import logo from '../images/logo.png';

export default function AppNavbar(){

    const { user } = useContext(UserContext);
    console.log(user);

    const [activeLink, setActiveLink] = useState('home');
  


  const onUpdateActiveLink = (value) => {
    setActiveLink(value);
  }  




    return(

        <Navbar expand="md">
        <Container>
            <Navbar.Brand as={ NavLink } to="/">
                <Row className="align-items-center">
                <Col><img src={ logo } alt=""/></Col><Col><h5 className="brand ff-bold">Shoes in Marikina</h5></Col>
                </Row>
                
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav">
                <span className="navbar-toggler-icon"></span>
            </Navbar.Toggle>
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto ff-reg">
                <Nav.Link as={ NavLink } to="/" end className={activeLink === 'home' ? 'active navbar-link' : 'navbar-link'} onClick={() => onUpdateActiveLink('home')}>Home</Nav.Link>
                <Nav.Link as={ NavLink } to="/products" end className={activeLink === 'products' ? 'active navbar-link' : 'navbar-link'} onClick={() => onUpdateActiveLink('products')}>Products</Nav.Link>

                {
                    (user.isAdmin === true) &&
                    <Nav.Link as={ NavLink } to="/dashboard" end className={activeLink === 'dashboard' ? 'active navbar-link' : 'navbar-link'} onClick={() => onUpdateActiveLink('dashboard')}>Dashboard</Nav.Link>
                }
                {
                    (user.isAdmin !== true && user.id !== null) &&
                    <Nav.Link as={ NavLink } to="/cart" end className={activeLink === 'cart' ? 'active navbar-link' : 'navbar-link'} onClick={() => onUpdateActiveLink('cart')}>Cart</Nav.Link>
                }
                {
                    (user.id !== null)
                    ?
                        <Nav.Link as={ NavLink } to="/logout" end className={activeLink === 'logout' ? 'active navbar-link' : 'navbar-link'} onClick={() => onUpdateActiveLink('logout')}>Logout</Nav.Link>
                       
                    :
                    <>
                        <Nav.Link as={ NavLink } to="/login" end className={activeLink === 'login' ? 'active navbar-link' : 'navbar-link'} onClick={() => onUpdateActiveLink('login')}>Login</Nav.Link>
                        <Nav.Link as={ NavLink } to="/register" end className={activeLink === 'register' ? 'active navbar-link' : 'navbar-link'} onClick={() => onUpdateActiveLink('register')}>Register</Nav.Link>
                    </>
                }
            </Nav>
            </Navbar.Collapse>
        </Container>
        </Navbar>


    
    )
}